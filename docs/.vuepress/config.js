module.exports = {
  title: 'Adam Nielsen',
  base: '/',
  description: 'My blog, for you and my future me',
  logo: './assets/img/logo.png',
  theme: require.resolve('../../'),
  themeConfig: {
  authors: [
      {
      name: 'Dr. Adam Nielsen',
      avatar: '/assets/img/adam.jpg',
      link: 'https://www.youtube.com/channel/UCB93o0rFpzmfzF4FKf6RTxg',
      linktext: 'Subscribe',
      },
    ],
    footer: {
      contact: [
        {
          type: 'codepen',
          link: '#',
        },
        {
          type: 'facebook',
          link: '#',
        },
        {
          type: 'github',
          link: 'https://github.com/iwasherefirst2',
        },
        {
          type: 'gitlab',
          link: '#',
        },
        {
          type: 'instagram',
          link: '#',
        },
        {
          type: 'linkedin',
          link: '#',
        },
        {
          type: 'mail',
          link: '#',
        },
        {
          type: 'messenger',
          link: '#',
        },
        {
          type: 'phone',
          link: '#',
        },
        {
          type: 'twitter',
          link: '#',
        },
        {
          type: 'web',
          link: '#',
        }
      ],
      copyright: [
        {
          text: 'Licensed MIT.',
          link: 'https://bootstrapstarter.com/license/',
        },
        {
          text: 'Made with Mediumish - free Vuepress theme',
          link: 'https://bootstrapstarter.com/bootstrap-templates/vuepress-theme-mediumish/',
        },
      ],
    },

    sitemap: {
      hostname: ''
    },
    comment: {
  
    },
    newsletter: {
      endpoint: '',
    },
    feed: {
      canonical_base: '',
    },
    smoothScroll: true
  },
}
