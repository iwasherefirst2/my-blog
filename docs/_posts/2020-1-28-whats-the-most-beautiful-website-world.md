---
title: Vue select one object in Multiselect
date: 2020-1-28
tags: 
  - vue
author: Dr. Adam Nielsen
featuredimg: 'https://images.unsplash.com/photo-1568777036071-f9a769596a49?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjE3MzYxfQ&auto=format&fit=crop&w=1351&q=80'
summary: 
---

When I use [multiselect](https://vue-multiselect.js.org/) I often end up having multiple option objects.

```vue
<multiselect 
  v-model="form.color"  
  track-by="name" 
  label="name"  
  :options="{{id:1, name:'red'}, {id:2, name:'blue'}}" >
</multiselect>
```

This will display all the colors in the select box. However the v-model “form.color” will be the full object. 
You may want that this is only the id.

One workaround would be to use id for the options and search for each id the name.


```vue
<multiselect v-model="form.color" 
:options="options.map(type => type.id)"
:custom-label="opt => options.find(x => x.id == opt).name" > 
</multiselect>
```

Another solution is to modify the `form` object on submit and replace `form.color` object by id. 

This is of course not very scalable, as for each item we need to have a search of `O(n²)`. Thus, this is causing serious performance iossues.


So its probably best to modify your “form” object and reduce “form.color” object to its id before moving on.

If this is no option for you, then you may try to use v-select, which has this feature build in:

```vue
<v-select 
  v-model="form.color"
  label="name"
  :reduce="item => item.id"
  :options="{{id:1, name:"red"}, {id:2, name:"blue"}}">
</v-select>
```