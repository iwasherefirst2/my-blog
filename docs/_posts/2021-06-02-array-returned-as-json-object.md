---
title: Array returned as json object
date: 2021-6-2
tags: 
  - laravel
  - javascript
author: Dr. Adam Nielsen
featuredimg: '/assets/img/thumbs/json-array-vs-obj.png'
summary: Be careful with your keys.
---

If you return an array in Laravel, it [converts into an json object]((https://laravel.com/docs/responses#strings-arrays)).

<center> 

![Laravel docs containing array-json section from above link](~@source/images/array-returnes-as-json/array-json-docs.png)

</center>

In a controller of my Laravel application I had the following code:

```php
return [ 
  'invitable' => 15,
  'noEmail' => $users->filter(function (User $user) {
                return $user->hasNoEmail();
               ])->toArray(),
  'noCertificate' =>  1                
];
```

Therefore I had the assumption, that this will return a json object where value `noEmail` shall be
an array. But for some reason, the value of `noEmail` was an array:


<center>

![Screenshot showing value of noEmail was an object](~@source/images/array-returnes-as-json/screenshot-returned-json.png)

</center>

Taking a look at the object shows

<center>

![Screenshot showing value of noEmail was an object in detail](~@source/images/array-returnes-as-json/screenshot-returned-json-detailed.png)

</center>

The problem was that because of the filter, the key indexes did change. In JS an array has to start with key 0. An array is not associative as it is in PHP (and actually, most languages have no associative array. those things would be named a hashmap).

So this means, if we receive this data in JS it will be an object and not an array. This can cause different conflicts. The most obvious one would be that the “length” attribute does not exists.

A simple solution to fix it, is by reindexing the collection at Laravel using the [values method](https://laravel.com/docs/8.x/collections#method-values). 

```php
return [ 
  'invitable' => 15,
  'noEmail' => $users->filter(function (User $user) {
                return $user->hasNoEmail();
               ])->values()->toArray(),
  'noCertificate' =>  1                
];
```
