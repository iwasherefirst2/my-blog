---
title: CORS and CSRF
date: 2020-1-1
tags: 
  - http
author: Sal
featuredimg: 'https://images.unsplash.com/photo-1576772269684-6c3b810ac8a9?ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80'
summary: All you need to know
---

All browsers implement the same-origin policy. This policy avoids in general that domain A can make a HTTP request to domain B.

The same-origin policy does not restrict embed tags like this:

```html
<img src=”https://dashboard.example.com/post-message/hello">
```

It’s irrelevant whether the response is a valid image — the request is still executed. This is why it’s important that state-changing endpoints on your web application cannot be invoked with the GET method.

For all other requests, you may use CORS to avoid same-origin policy and let the domain A make a request to domain B that would otherwise be forbidden. Before the actually request is send, a preflight request will be send to check if the server allows domain A to send this request type. If it does, domain A will send the original request.

For example, if no CORS are set, then a Javascript XMLHttpRequests would be restricted for domain A.

But there is one big problem. GET, HEAD and POST requests with specific headers and specific content-type do not send a preflight request. Such requests are called simple requests. This means the request will be executed and if the request was not allowed, then a not-allowed error response will be returned. But the problem is, that the simple request was executed on the server.

Here is a cool article that helped me a lot to grasp the topic: https://poshai.medium.com/are-csrf-tokens-necessary-3a6976bf1f34

https://stackoverflow.com/questions/24680302/csrf-protection-with-cors-origin-header-vs-csrf-token

https://stackoverflow.com/questions/19793695/does-a-proper-cors-setup-prevent-xsrf

