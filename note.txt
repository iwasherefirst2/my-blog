Excellent post 
https://greywyvern.com/?post=337


FormData

You can send with HTML two types of content-type, multipart / form-data for file uploads and application/x-www-form-urlencoded for simple key-value pairs. Here the body of the form is url encoded. (see https://dev.to/getd/x-www-form-urlencoded-or-form-data-explained-in-2-mins-5hk6 how the encodings look like)

If you use fetch, then you don't need to specify the content-type (see https://stackoverflow.com/questions/68643330/pass-data-to-service-in-axios/68643919#68643919)


Related: 
https://stackoverflow.com/questions/4007969/application-x-www-form-urlencoded-or-multipart-form-data

This video is cool, but guy doesn't know how to pass formdata in URLSearchParams
